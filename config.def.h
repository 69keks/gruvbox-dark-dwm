/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const int startwithgaps	    = 1;	/* 1 means gaps are used by default */
static const unsigned int gappx     = 12;       /* default gap between windows in pixels */
static const unsigned int snap      = 32;       /* snap pixel */
static const int scalepreview       = 4;        /* tag preview scaling */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const double activeopacity   = 1.0f;	/* Window opacity on focus (0 <= opacity <= 1) */
static const double inactiveopacity = 0.875f;	/* Window opacity when out of focus (0 <= opacity <= 1) */
static       Bool bUseOpacity	    = True;	/* Starts with opacity on unfocused windows */
static const int vertpad	    = 10;	/* vertical padding of bar */
static const int sidepad	    = 10;	/* horizontal padding of bar */
static const int user_bh	    = 26;	/* 0 is default, >=1 means dwm will use this value as bar height */
static const char *fonts[]          = { "Cascadia Mono:size=14:style=Regular:autohint=true:antialias=true",
	"CaskaydiaCove Nerd Font Mono:size=21:autohint=true:antialias:true" };
static const char dmenufont[]       = "CaskaydiaCove Nerd Font Mono:size=13";
static const char col_gray1[]       = "#1E1E2E";
static const char col_gray2[]       = "#1E1E2E";
static const char col_gray3[]       = "#D9E0EE";
static const char col_gray4[]       = "#D9E0EE";
static const char col_cyan[]        = "#F28FAD";
static const char *colors[][3]      = {
	/*                       fg          bg       border   */
	[SchemeNorm]     =  { col_gray3,  col_gray1, col_gray2  },
	[SchemeSel]      =  { col_gray4,  col_gray2, col_gray1  },
	[SchemeStatus]   =  { col_gray3,  col_gray1, "#000000"  },
	[SchemeTagsSel]  =  { col_gray4,  col_gray2, "#000000"  },
	[SchemeTagsNorm] =  { col_gray3,  col_gray1, "#000000"  },
	[SchemeInfoSel]  =  { col_gray4,  col_gray1, "#000000"  },
	[SchemeInfoNorm] =  { col_gray3,  col_gray1, "#000000"  },
};

static const char *const autostart[] = {
	"feh", "--bg-fill", "/home/teri/wm/dwm/wallpapers/cabin-in-the-woods.png", NULL,
	"picom", "--experimental-backends", NULL,
	"dwmblocks", NULL,
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "" };

static const char *tagsel[][2] = {
	{ "#000000", "#ABE9B3" },
	{ "#000000", "#96CDFB" },
	{ "#000000", "#FAE3B0" },
	{ "#000000", "#DDB6F2" },
	{ "#000000", "#F28FAD" },
	{ "#000000", "#B5E8E0" },
	{ "#000000", "#9400d3" },
	{ "#000000", "#ffffff" },
	{ "#ffffff", "#000000" },
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Zathura",  NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Chromium",    NULL,       NULL,       1 << 1,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.53; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray1, NULL };
static const char *termcmd[]  = { "st", NULL };

/* brightness */
static const char *brightness_down[]    = { "light", "-U", "3", NULL };
static const char *brightness_up[]      = { "light", "-A", "3", NULL };

/* keyboard backlight */
static const char *kbdbrightness_up[]   = { "asusctl", "-n", NULL };
static const char *kbdbrightness_down[] = { "asusctl", "-p", NULL };

/* Volume Control */
static const char *volup[]      = { "amixer", "sset", "Master", "5%+", NULL };
static const char *voldown[]    = { "amixer", "sset", "Master", "5%-", NULL };
static const char *mutevol[]    = { "amixer", "sset", "Master", "toggle", NULL };

/* App key-bindings */
static const char *browsercmd[] = { "chromium", NULL };
static const char *pdfviewcmd[] = { "zathura", NULL };
static const char *emacscmd[]	= { "emacs", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */

	/*                          App Bindings                            */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd   } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd    } },
	{ ControlMask|ShiftMask,        XK_b,      spawn,          {.v = browsercmd } },
	{ ControlMask|ShiftMask,        XK_z,      spawn,          {.v = pdfviewcmd } },
	{ ControlMask|ShiftMask,	XK_e,	   spawn,	   {.v = emacscmd   } },

	/*                          Dwm Bindings                            */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_a,      toggleopacity,  {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} }, 

	/*                          Gap Bindings                            */
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = GAP_TOGGLE} },

	/*                        XF86 Key Bindings                         */
	{ 0,                            XF86XK_MonBrightnessUp, spawn,  {.v = brightness_up } },
	{ 0,                            XF86XK_MonBrightnessDown,       spawn,  {.v = brightness_down } },
	{ 0,                            XF86XK_KbdBrightnessUp, spawn,  {.v = kbdbrightness_up } },
	{ 0,                            XF86XK_KbdBrightnessDown,       spawn, {.v = kbdbrightness_down } },
	{ 0,                            XF86XK_AudioRaiseVolume,        spawn, {.v = volup } },
	{ 0,                            XF86XK_AudioLowerVolume,        spawn, {.v = voldown } },
	{ 0,                            XF86XK_AudioMute,               spawn, {.v = mutevol } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

